<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->all();
        // dd($data);

        $rules = [];
        $rules['data.api.app_id'] = 'required';
        $rules['data.api.app_secret'] = 'required';

        $rules['data.consignment_prefix'] = 'required|min:4|max:6';

        $rules['data.sender_details.name'] = 'required';
        $rules['data.sender_details.email'] = 'required|email';
        $rules['data.sender_details.mobile_number'] = 'required|digits:10';
        $rules['data.sender_details.address1'] = 'required';
        // $rules['data.sender_details.address2'] = 'required';
        $rules['data.sender_details.postal_code'] = 'required|digits:5';
        $rules['data.sender_details.city'] = 'required';
        $rules['data.sender_details.state'] = 'required';

        return $rules;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(){
        $rules = [];
        $rules['data.api.app_id.*'] = 'App id is required.';
        $rules['data.api.app_secret.*'] = 'App secret is required.';

        $rules['data.consignment_prefix.*'] = 'Consignment Prefix field is required and must be within 4 and 6 characters.s';

        $rules['data.sender_details.name.*'] = 'Sender Name is required.';
        $rules['data.sender_details.email.required'] = 'Sender email is required.';
        $rules['data.sender_details.email.email'] = 'Sender email must be valid email address.';

        $rules['data.sender_details.mobile_number.required'] = 'Mobile number field is required.';
        $rules['data.sender_details.mobile_number.digits'] = 'Mobile number length should be 10.';

        $rules['data.sender_details.address1.*'] = 'Address 1 field is required.';
        // $rules['data.sender_details.address2.*'] = 'Address 2 field is required.';

        $rules['data.sender_details.postal_code.required'] = 'Postal code is required.';
        $rules['data.sender_details.postal_code.digits'] = 'Postal code length should be 5.';

        $rules['data.sender_details.city.*'] = 'Sender City is required.';
        $rules['data.sender_details.state.*'] = 'Sender State is required.';

        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }

}
