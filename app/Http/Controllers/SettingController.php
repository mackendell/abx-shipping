<?php

namespace App\Http\Controllers;

use App\Http\Requests\SettingRequest;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    public function index()
    {

        try {
            $user_id = Auth::user()->id;

            $settings = Setting::where('user_id', $user_id)->first();
            // dd($settings);
            return response()->json([
                'success' => true,
                'data' => json_decode($settings->data)
            ], 200);

        } catch (\Throwable $e) {
            return response()->json([
                'success' => false,
                'data' => $e 
            ], 422);
        }
    }

    public function saveSettings(SettingRequest $request){
        try {
            
            $user_id = Auth::user()->id;
            $settings = Setting::where('user_id', $user_id)->first();
            $settings->data = json_encode($request->data);
            $settings->save();

            return response()->json([
                'success' => true,
                'data' => 'Data Saved successfully.'
            ], 200);

        } catch (\Throwable $e) {
            return response()->json([
                'success' => false,
                'data' => $e 
            ], 422);
        }
    }
}
