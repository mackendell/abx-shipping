<?php

namespace App\Http\Controllers;

use App\Models\Consignment;
use App\Models\Setting;
use App\Traits\GraphQLTrait;
use Carbon\Carbon;
use GrahamCampbell\ResultType\Success;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class ConsignmentController extends Controller
{
    use GraphQLTrait;

    public function make_consignments(Request $request)
    {
        try {
            if (isset($request->id)) {
                $orderIds = $request->id;
                $orderIds = [$orderIds];
            } else {
                $orderIds = $request->ids;
            }
            // dd($orderIds);
            $settings = Setting::where('user_id', Auth::user()->id)->first();
            $settingData = json_decode($settings->data);
            // dd($settingData);

            if (
                $settingData->api->app_id != ''
                && $settingData->api->app_secret != ''
                && $settingData->sender_details->city != ''
                && $settingData->sender_details->name != ''
                && $settingData->sender_details->state != ''
                && $settingData->sender_details->address1 != ''
                && $settingData->sender_details->postal_code != ''
                && $settingData->sender_details->mobile_number != ''
                && $settingData->consignment_prefix != ''
            ) {
                $shop = Auth::user();

                $shop_data = $shop->api()->rest('GET', '/admin/api/2022-01/shop.json');
                $location_id = $shop_data['body']['container']['shop']['primary_location_id'];
                // dd($location_id);
                $isFailed = 0;
                $failedOrders = [];
                foreach ($orderIds as $orderId) {
                    $shop = Auth::user();
                    $order = $shop->api()->rest('GET', '/admin/api/2022-01/orders/' . $orderId . '.json');

                    if ($order['errors'] == false) {
                        $order = $order['body']['container']['order'];
                        // dd($order);
                        $exist = Consignment::where('order_number', $order['order_number'])->where('user_id', Auth::user()->id)->first();

                        // dd($order);
                        if (!isset($exist) && isset($order['customer']) && $order['cancelled_at'] == null && $order['fulfillment_status'] != 'fulfilled' && isset($order['shipping_address'])) {

                            $splitOrder = false;
                            $locations = [];
                            // dd($order['line_items']);
                            // foreach ($order['line_items'] as $i => $line_item) {
                            //     if ($line_item['requires_shipping'] == 'true') {
                            //         // dump($line_item['id']);
                            //         $var_id = $line_item['id'];
                            //         $query = '{
                            //             productVariant(id: "gid://shopify/ProductVariant/' . $var_id . '") {
                            //             inventoryItem {
                            //                 inventoryLevels(first: 10) {
                            //                 edges {
                            //                     node {
                            //                     location {
                            //                         id
                            //                     }
                            //                     }
                            //                 }
                            //                 }
                            //             }
                            //             }
                            //         }';
                            //         // $var_data = $this->graph($shop,$query);

                            //         $var_data = $shop->api()->graph($query);
                            //         $var_data = $var_data['body']->container['data']['productVariant'];
                            //         // dd($var_data);

                            //         $loc_id = $var_data['inventoryItem']['inventoryLevels']['edges'][0]['node']['location']['id'];


                            //         if (array_key_exists($loc_id, $locations)) {
                            //             $locations[$loc_id][] = $var_id;
                            //         } else {
                            //             $locations[$loc_id] = [$var_id];
                            //         }
                            //     }
                            // }


                            $query = '{
                                order(id: "' . $order['admin_graphql_api_id'] . '") {
                                  fulfillmentOrders(first: 10) {
                                    edges {
                                      node {
                                        lineItems(first: 10) {
                                          edges {
                                            node {
                                              lineItem {
                                                id
                                                variantTitle
                                              }
                                            }
                                          }
                                        }
                                        assignedLocation {
                                          location {
                                            id
                                          }
                                        }
                                      }
                                    }
                                  }
                                  email
                                  id
                                  lineItems(first: 10) {
                                    edges {
                                      node {
                                        id
                                      }
                                    }
                                  }
                                }
                              }
                              ';


                            $var_data = $shop->api()->graph($query);
                            $location_data = $var_data['body']->container['data']['order']['fulfillmentOrders']['edges'];
                            // dd($var_data);

                            // $loc_id = $var_data['inventoryItem']['inventoryLevels']['edges'][0]['node']['location']['id'];
                            foreach ($location_data as $loc_data) {
                                // dd($loc_data);
                                $line_items_data = $loc_data['node']['lineItems']['edges'];
                                $loc_id = $loc_data['node']['assignedLocation']['location']['id'];

                                foreach ($line_items_data as $l_item) {

                                    $l_item = str_replace("gid://shopify/LineItem/", "", $l_item['node']['lineItem']['id']);
                                    $l_item = (int)$l_item;

                                    $locations[$loc_id][] = $l_item;
                                }
                            }

                            // dd($locations);
                            // dd(count($locations));

                            if (count($locations) > 0) {
                                Session::put('success', 'One or more selected item is fulfilled, please ensure no fulfilled order is selected');
                                Session::save();
                                $total_result = [];
                                foreach ($locations as $key => $location) {
                                    $weight = 0;
                                    $str = '';
                                    $line_items = [];
                                    $fullfilled_line_items = [];
                                    foreach ($order['line_items'] as $i => $line_item) {
                                        if (in_array($line_item['id'], $location)) {
                                            $weight = $weight + $line_item['grams'];

                                            if ($i == 0) {
                                                $str = $str . (string)$line_item['title'];
                                            } else {
                                                $str = $str .  ',' . (string)$line_item['title'];
                                            }

                                            if ($line_item['fulfillable_quantity'] != 0) {
                                                $line_items[] = ['id' => $line_item['id']];
                                            } else {
                                                $fullfilled_line_items[] = ['id' => $line_item['id']];
                                            }
                                        }
                                    }
                                    $str = substr($str, 0, 20);
                                    $apiData = [];
                                    $apiData['ref_no'] = $str;

                                    $weight = $weight > 0.5 ? ($weight / 1000) : 0.5;
                                    $total_package = explode(".", round($weight / 8))[0];
                                    $total_package = $total_package < 1 ? 1 : $total_package;


                                    $apiData['con_no'] = $settingData->consignment_prefix . rand(000000000, 999999999);
                                    $apiData['s_name'] = $settingData->sender_details->name;
                                    $apiData['s_address'] = $settingData->sender_details->address1 . ' , ' . $settingData->sender_details->address2 . ' , ' . $settingData->sender_details->city . ' , ' . $settingData->sender_details->state;
                                    $apiData['s_zipcode'] = $settingData->sender_details->postal_code;
                                    $apiData['s_mobile1'] = $settingData->sender_details->mobile_number;

                                    if (
                                        isset($order['shipping_address']['first_name']) &&
                                        ($order['shipping_address']['first_name'] != null || $order['shipping_address']['first_name'] != '') &&

                                        isset($order['shipping_address']['last_name']) &&
                                        ($order['shipping_address']['last_name'] != null || $order['shipping_address']['last_name'] != '') &&

                                        isset($order['shipping_address']['address1']) &&
                                        ($order['shipping_address']['address1'] != null || $order['shipping_address']['address1'] != '') &&

                                        isset($order['shipping_address']['zip']) &&
                                        ($order['shipping_address']['zip'] != null || $order['shipping_address']['zip'] != '') &&

                                        isset($order['shipping_address']['phone']) &&
                                        ($order['shipping_address']['phone'] != null || $order['shipping_address']['phone'] != '')
                                    ) {

                                        $apiData['r_name'] = $order['shipping_address']['first_name'] . ' ' . $order['shipping_address']['last_name'];
                                        $apiData['r_address'] = $order['shipping_address']['address1'] . ',' . $order['shipping_address']['address2'] . ' , ' . $order['shipping_address']['city'] . ' , ' . $order['shipping_address']['province'];
                                        $apiData['r_zipcode'] = $order['shipping_address']['zip'];
                                        $apiData['r_mobile1'] = $order['shipping_address']['phone'];

                                        $apiData['service_code'] = 'ND';
                                        $apiData['tot_pkg'] = $total_package;
                                        $apiData['weight'] = $weight;

                                        $apiData = [
                                            'req' => [
                                                'shipment' => $apiData,
                                                'order_number' => $order['order_number'],
                                                'date' => Carbon::now()
                                            ]
                                        ];

                                        $apiData = json_encode($apiData);
                                        $headers = [
                                            "app_id: " . $settingData->api->app_id,
                                            "app_key: " . $settingData->api->app_secret,
                                            "content-type: application/json",
                                        ];

                                        $url = "https://kexsystem.com.my/KerrySmartEDI/SmartEDI/shipment_info";

                                        $response = $this->curlHelper($url, $apiData, $headers);
                                        // dd($response);

                                        if (json_decode($response)->res->shipment->status_code == 000) {

                                            $key = str_replace("gid://shopify/Location/", "", $key);
                                            $key = (int)$key;

                                            $shop = Auth::user();

                                            $line_items = array_reverse($line_items);
                                            $fullfill_data = [
                                                "fulfillment" => [
                                                    "location_id" => $key,
                                                    "tracking_number" => json_decode($response)->res->shipment->con_no,
                                                    "tracking_url" => "https://kexexpress.com.my/Home/Track?trackingInfo=" . json_decode($response)->res->shipment->con_no,
                                                    "tracking_company" => "ABX",
                                                    "line_items" => $line_items,
                                                ]
                                            ];
                                            $fullfill_status = $shop->api()->rest('POST', '/admin/api/2021-10/orders/' . $order['id'] . '/fulfillments.json', $fullfill_data);
                                            // $total_result[] = $fullfill_status;

                                            // if (count($fullfilled_line_items) > 0) {
                                            foreach ($order['fulfillments'] as $fullfillment) {
                                                if ($fullfillment['location_id'] == $key) {
                                                    $fullfill_data = [
                                                        "fulfillment" => [
                                                            "id" => $fullfillment['id'],
                                                            "tracking_number" => json_decode($response)->res->shipment->con_no,
                                                            "tracking_url" => "https://kexexpress.com.my/Home/Track?trackingInfo=" . json_decode($response)->res->shipment->con_no,
                                                            "tracking_company" => "ABX",
                                                        ]
                                                    ];

                                                    $fullfill_status = $shop->api()->rest('PUT', '/admin/api/2021-10/orders/' . $order['id'] . '/fulfillments/' . $fullfillment['id'] . '.json', $fullfill_data);
                                                }
                                            }

                                            $consignments = new Consignment();
                                            $consignments->user_id = Auth::user()->id;
                                            $consignments->response_data = $response;
                                            $consignments->data = $apiData;
                                            $consignments->status = json_decode($response)->res->shipment->status_code;
                                            $consignments->con_number = json_decode($response)->res->shipment->con_no;
                                            $consignments->order_number = $order['order_number'];
                                            $consignments->order_id = $order['id'];
                                            $consignments->location_id = $key;
                                            $consignments->save();
                                            // }
                                        } else {
                                            Session::forget('success');
                                            $isFailed++;
                                            $failedOrders[] = $order['order_number'];
                                            Session::put('error', json_decode($response)->res->shipment->status_desc);
                                            Session::save();
                                        }
                                    } else {
                                        $isFailed++;
                                        $failedOrders[] = $order['order_number'];
                                        Session::forget('success');
                                        Session::put('error', 'One or more selected order does not have sufficient shipping data.');
                                        Session::save();
                                    }
                                }
                                // dd($total_result);
                                // dd('end');
                            } else {
                                // dd('less locations');

                                Session::put('success', 'One or more selected item is fulfilled, please ensure no fulfilled order is selected');
                                Session::save();

                                $weight = $order['total_weight'] > 0.5 ? ($order['total_weight'] / 1000) : 0.5;
                                $total_package = explode(".", round($weight / 8))[0];
                                $total_package = $total_package < 1 ? 1 : $total_package;

                                $apiData = [];

                                $apiData['con_no'] = $settingData->consignment_prefix . rand(000000000, 999999999);
                                $apiData['s_name'] = $settingData->sender_details->name;
                                $apiData['s_address'] = $settingData->sender_details->address1 . ' , ' . $settingData->sender_details->address2 . ' , ' . $settingData->sender_details->city . ' , ' . $settingData->sender_details->state;
                                $apiData['s_zipcode'] = $settingData->sender_details->postal_code;
                                $apiData['s_mobile1'] = $settingData->sender_details->mobile_number;
                                // dd($order);

                                if (
                                    isset($order['shipping_address']['first_name']) &&
                                    ($order['shipping_address']['first_name'] != null || $order['shipping_address']['first_name'] != '') &&

                                    isset($order['shipping_address']['last_name']) &&
                                    ($order['shipping_address']['last_name'] != null || $order['shipping_address']['last_name'] != '') &&

                                    isset($order['shipping_address']['address1']) &&
                                    ($order['shipping_address']['address1'] != null || $order['shipping_address']['address1'] != '') &&

                                    isset($order['shipping_address']['zip']) &&
                                    ($order['shipping_address']['zip'] != null || $order['shipping_address']['zip'] != '') &&

                                    isset($order['shipping_address']['phone']) &&
                                    ($order['shipping_address']['phone'] != null || $order['shipping_address']['phone'] != '')
                                ) {
                                    $apiData['r_name'] = $order['shipping_address']['first_name'] . ' ' . $order['shipping_address']['last_name'];
                                    $apiData['r_address'] = $order['shipping_address']['address1'] . ',' . $order['shipping_address']['address2'] . ' , ' . $order['shipping_address']['city'] . ' , ' . $order['shipping_address']['province'];
                                    $apiData['r_zipcode'] = $order['shipping_address']['zip'];
                                    $apiData['r_mobile1'] = $order['shipping_address']['phone'];

                                    $apiData['service_code'] = 'ND';
                                    $apiData['tot_pkg'] = $total_package;
                                    $apiData['weight'] = $weight;

                                    $str = '';
                                    foreach ($order['line_items'] as $i => $lineitem) {
                                        if ($i == 0) {
                                            $str = $str . (string)$lineitem['title'];
                                        } else {
                                            $str = $str .  ',' . (string)$lineitem['title'];
                                        }
                                    }
                                    $str = substr($str, 0, 20);
                                    $apiData['ref_no'] = $str;

                                    $apiData = [
                                        'req' => [
                                            'shipment' => $apiData,
                                            'order_number' => $order['order_number'],
                                            'date' => Carbon::now()
                                        ]
                                    ];
                                    $apiData = json_encode($apiData);

                                    $headers = [
                                        "app_id: " . $settingData->api->app_id,
                                        "app_key: " . $settingData->api->app_secret,
                                        "content-type: application/json",
                                    ];

                                    $url = "https://kexsystem.com.my/KerrySmartEDI/SmartEDI/shipment_info";

                                    $response = $this->curlHelper($url, $apiData, $headers);
                                    // dd($response);

                                    if (json_decode($response)->res->shipment->status_code == 000) {
                                        $shop = Auth::user();

                                        $line_items = [];
                                        foreach ($order['line_items'] as $item) {
                                            if ($item['fulfillable_quantity'] != 0) {
                                                $line_items[] = ['id' => $item['id']];
                                            }
                                        }
                                        $line_items = array_reverse($line_items);
                                        $fullfill_data = [
                                            "fulfillment" => [
                                                "location_id" => $location_id,
                                                "tracking_number" => json_decode($response)->res->shipment->con_no,
                                                "tracking_url" => "https://kexexpress.com.my/Home/Track?trackingInfo=" . json_decode($response)->res->shipment->con_no,
                                                "tracking_company" => "ABX",
                                                "line_items" => $line_items,
                                            ]
                                        ];

                                        $fullfill_status = $shop->api()->rest('POST', '/admin/api/2021-10/orders/' . $order['id'] . '/fulfillments.json', $fullfill_data);

                                        $apiData = json_decode($apiData);
                                        $apiData->order_id = $order['id'];
                                        $apiData = json_encode($apiData);

                                        $consignments = new Consignment();
                                        $consignments->user_id = Auth::user()->id;
                                        $consignments->response_data = $response;
                                        $consignments->data = $apiData;
                                        $consignments->status = json_decode($response)->res->shipment->status_code;
                                        $consignments->con_number = json_decode($response)->res->shipment->con_no;
                                        $consignments->order_number = $order['order_number'];
                                        $consignments->order_id = $order['id'];
                                        $consignments->location_id = $location_id;
                                        $consignments->save();
                                        // dd($fullfill_status);
                                    } else {
                                        Session::forget('success');
                                        $isFailed++;
                                        $failedOrders[] = $order['order_number'];
                                        Session::put('error', json_decode($response)->res->shipment->status_desc);
                                        Session::save();

                                        // if (Session::has('error')) {
                                        //     return view('app');
                                        // } else {
                                        //     return redirect('/');
                                        // }
                                    }
                                } else {
                                    $isFailed++;
                                    $failedOrders[] = $order['order_number'];
                                    Session::forget('success');
                                    Session::put('error', 'One or more selected order does not have sufficient shipping data.');
                                    Session::save();
                                }
                            }
                        } else {

                            if (isset($exist) && $order['fulfillment_status'] != 'fulfilled') {
                                $exists = Consignment::where('order_number', $order['order_number'])->where('user_id', Auth::user()->id)->get();
                                foreach ($exists as $exist) {

                                    $locations = [];
                                    $query = '{
                                        order(id: "' . $order['admin_graphql_api_id'] . '") {
                                          fulfillmentOrders(first: 10) {
                                            edges {
                                              node {
                                                lineItems(first: 10) {
                                                  edges {
                                                    node {
                                                      lineItem {
                                                        id
                                                        variantTitle
                                                      }
                                                    }
                                                  }
                                                }
                                                assignedLocation {
                                                  location {
                                                    id
                                                  }
                                                }
                                              }
                                            }
                                          }
                                          email
                                          id
                                          lineItems(first: 10) {
                                            edges {
                                              node {
                                                id
                                              }
                                            }
                                          }
                                        }
                                      }
                                      ';

                                    $var_data = $shop->api()->graph($query);
                                    $location_data = $var_data['body']->container['data']['order']['fulfillmentOrders']['edges'];

                                    foreach ($location_data as $loc_data) {
                                        $line_items_data = $loc_data['node']['lineItems']['edges'];
                                        $loc_id = $loc_data['node']['assignedLocation']['location']['id'];

                                        foreach ($line_items_data as $l_item) {
                                            $l_item = str_replace("gid://shopify/LineItem/", "", $l_item['node']['lineItem']['id']);
                                            $l_item = (int)$l_item;
                                            $locations[$loc_id][] = $l_item;
                                        }
                                    }

                                    // dd($locations);
                                    if (count($locations) > 0) {
                                        foreach ($locations as $key => $location) {
                                            $line_items = [];
                                            $fullfilled_line_items = [];
                                            foreach ($order['line_items'] as $i => $line_item) {
                                                if (in_array($line_item['id'], $location)) {

                                                    if ($line_item['fulfillable_quantity'] != 0) {
                                                        $line_items[] = ['id' => $line_item['id']];
                                                    } else {
                                                        $fullfilled_line_items[] = ['id' => $line_item['id']];
                                                    }
                                                }
                                            }

                                            $shop = Auth::user();

                                            $line_items = array_reverse($line_items);
                                            $key = str_replace("gid://shopify/Location/", "", $key);
                                            $key = (int)$key;

                                            $fullfill_data = [
                                                "fulfillment" => [
                                                    "location_id" => $key,
                                                    "tracking_number" => $exist->con_number,
                                                    "tracking_url" => "https://kexexpress.com.my/Home/Track?trackingInfo=" . $exist->con_number,
                                                    "tracking_company" => "ABX",
                                                    "line_items" => $line_items,
                                                ]
                                            ];
                                            $fullfill_status = $shop->api()->rest('POST', '/admin/api/2021-10/orders/' . $order['id'] . '/fulfillments.json', $fullfill_data);

                                            Session::put('success', 'One or more selected item is fulfilled, please ensure no fulfilled order is selected');
                                            Session::save();
                                        }
                                    } else {
                                        $shop = Auth::user();

                                        $line_items = [];
                                        foreach ($order['line_items'] as $item) {
                                            if ($item['fulfillable_quantity'] != 0) {
                                                $line_items[] = ['id' => $item['id']];
                                            }
                                        }
                                        $line_items = array_reverse($line_items);
                                        $fullfill_data = [
                                            "fulfillment" => [
                                                "location_id" => $location_id,
                                                "tracking_number" => $exist->con_number,
                                                "tracking_url" => "https://kexexpress.com.my/Home/Track?trackingInfo=" . $exist->con_number,
                                                "tracking_company" => "ABX",
                                                "line_items" => $line_items,
                                            ]
                                        ];
                                        $fullfill_status = $shop->api()->rest('POST', '/admin/api/2021-10/orders/' . $order['id'] . '/fulfillments.json', $fullfill_data);
                                        Session::put('success', 'One or more selected item is fulfilled, please ensure no fulfilled order is selected');
                                        Session::save();
                                    }
                                }
                            } else {
                                // Session::forget('success');
                                $isFailed++;
                                $failedOrders[] = $order['order_number'];
                                if (isset($exist)) {
                                    // Session::put('error', 'Consignment already exist of one or more selected order.');
                                    // Session::save();
                                } else if (!isset($order['customer'])) {
                                    Session::put('error', 'Customer does not exist in one or more selected order.');
                                    Session::save();
                                } else if ($order['fulfillment_status'] == 'fulfilled') {
                                    Session::put('error', 'Order is fulfilled but some of the order already has the consignment.');
                                    Session::save();
                                } else if ($order['cancelled_at'] != null) {
                                    Session::put('error', 'One or more selected order is Cancelled.');
                                    Session::save();
                                } else if (!isset($order['shipping_address'])) {
                                    Session::put('error', 'One or more selected order does not have shipping address.');
                                    Session::save();
                                }
                            }
                        }
                    }
                }

                if (count($orderIds) > 1) {
                    // dd($failedOrders);
                    Session::forget('success');
                    Session::forget('error');
                    if (count($orderIds) == $isFailed && $isFailed != 0) {
                        Session::put('error', 'All order are failed to create consignment.');
                        Session::save();
                    } elseif ($isFailed > 0) {
                        Session::put('success', 'Consignment has been created but for some order are failed. Here is the order number : ' . implode(",", $failedOrders));
                        Session::save();
                    } else {
                        Session::put('success', 'All order consignment has been created.');
                        Session::save();
                    }
                }
            } else {
                // if (Session::has('success')) {
                //     Session::forget('success');
                // }
                if (
                    $settingData->api->app_id == ''
                    && $settingData->api->app_secret == ''
                    && $settingData->sender_details->city == ''
                    && $settingData->sender_details->name == ''
                    && $settingData->sender_details->state == ''
                    && $settingData->sender_details->address1 == ''
                    && $settingData->sender_details->postal_code == ''
                    && $settingData->sender_details->mobile_number == ''
                    && $settingData->consignment_prefix == ''
                ) {
                    Session::put('error', 'Please fill the settings data.');
                    Session::save();
                }
            }

            if (Session::has('success') || Session::has('error')) {
                return view('app');
            } else {
                return redirect('/');
            }
        } catch (\Throwable $th) {
            logger("============= ERROR :: make_consignments =============");
            // logger($th);
            throw $th;
            response()->json([
                'success' => false,
                'error' => $th->getMessage()
            ], 422);
        }
    }

    public function index(Request $request)
    {
        try {

            $consignments = Consignment::query()
                            ->when($request->dateFilter, function ($query) use ($request) {
                                if ($request->dateFilter != '-') {
                                    $fromTo = explode(',', $request->dateFilter);

                                    $startDate = Carbon::parse($fromTo[0])->format('Y-m-d');
                                    $endDate = Carbon::parse($fromTo[1])->format('Y-m-d');

                                    return $query->whereDate('created_at', '>=', $startDate)
                                                ->whereDate('created_at', '<=', $endDate);
                                }

                            })
                            ->when($request->search, function ($query) use ($request) {
                                // return $query->where('data->req->shipment->r_name', 'LIKE', "{$request->search}%")
								//	->orWhere('con_number', 'LIKE', "{$request->search}%");

								return $query->whereRaw('LOWER(data) like ?', ['%' . strtolower($request->search) . '%'])
										->orWhere('con_number', 'LIKE', "{$request->search}%");
                            })
                            ->where('user_id', Auth::user()->id)
                            ->orderBy('created_at', 'desc')
                            ->paginate($request->limitShow);

            $data = [];

            foreach ($consignments as $consignment) {
                $consignment->data = json_decode($consignment->data);
                // dd($consignment);
                $consignment->data->req->date = Carbon::parse($consignment->data->req->date)->format('M d Y');
                $consignment->data->order_id = $consignment->order_id;
                $data[] = $consignment->data;
            }
            if ($consignments) {
                return response()->json([
                    'success' => true,
                    'data' => [
                        'consignments' => $data,
                        'pagination' => $consignments,
                    ]
                ], 200);
            }
        } catch (\Throwable $th) {
            throw $th;
            response()->json([
                'success' => false,
                'error' => $th
            ], 422);
        }
    }

    public function curlHelper($url, $data, $headers)
    {
        try {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $headers,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return ($err);
            } else {
                return ($response);
            }
        } catch (\Throwable $th) {
            throw $th;

            // CURLOPT_POSTFIELDS => "{\r\n    \"req\": {\r\n        \"shipment\": {\r\n            \"con_no\": \"KEX00000000992\",\r\n            \"s_name\": \"ABX EXPRESS (M) SDN BHD\",\r\n            \"s_address\": \"Lot 651, 1st Floor\",\r\n            \"s_zipcode\": \"47600\",\r\n            \"s_mobile1\": \"0812345678\",\r\n            \"r_name\": \"Hospital Shah Alam\",\r\n            \"r_address\": \"Persiaran Kayangan, Seksyen 7, 40000 Shah Alam, Selangor\",\r\n            \"r_zipcode\": \"40000\",\r\n            \"r_mobile1\": \"0819990012\",\r\n            \"service_code\": \"ND\",\r\n            \"tot_pkg\": 2,\r\n            \"weight\": 1.72\r\n        }\r\n    }\r\n}",

            // response()->json([
            //     'success' => false,
            //     'error' => $th
            // ], 422);
        }
    }

    public function noSession()
    {
        return redirect('/');
    }
}
