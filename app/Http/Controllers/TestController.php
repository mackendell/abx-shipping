<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index(){

        $user = User::find(1);

        $res = $user->api()->rest('GET' , '/admin/api/webhooks.json');
        dd($res);
    }
}
