<?php

namespace App\Jobs;

use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $shopDomain;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shopDomain)
    {
        $this->shopDomain = $shopDomain;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger('...........................................installation successfull.............................................');
        $user = $this->shopDomain;
        logger($user);

        $data = [
            "api" => [
                'app_id' => '',
                'app_secret' => ''
            ],
            "consignment_prefix" => '',
            "sender_details" => [
                'name' => '',
                'email' => '',
                'mobile_number' => '',
                'address1' => '',
                'address2' => '',
                'postal_code' => '',
                'city' => '',
                'state' => ''
            ],
        ];

        $setting = new Setting();
        $setting->user_id = $user->id;
        $setting->data = json_encode($data);
        $setting->save();


    }
}
