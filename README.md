# Shopify Metafield-Advance App

### Installation

git clone https://ruchi9391@bitbucket.org/crawlapps_stage/abx-shipping.git

Install the dependencies and devDependencies.

For both environment
```sh
$ composer install
$ cp .env.example .env 
$ nano .env // set all credentials(ex: database, shopify api key and secret)
$ php artisan migrate
```

For development environments...

```sh
$ npm install
$ npm run dev
```
For production environments...

```sh
$ npm install --production
$ npm run prod
```
extra commands

```shell script
php artisan storage:link
```

Update env file :
    - SHOPIFY_API_KEY=
    - SHOPIFY_API_SECRET=

Create queue supervisors.

### Used Shopify Tools

* Admin rest-api
* App-bridge
