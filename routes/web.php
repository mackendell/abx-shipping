<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\ConsignmentController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/login', [AuthController::class, 'index']);

Route::middleware(['verify.shopify','secure.headers'])->group(function () {

    Route::get('/', function () {
        return view('app');
    })->where('any', '.*')->name('home');

    Route::get('/settings', [SettingController::class, 'index'])->name('settings');
    Route::post('/saveSettings', [SettingController::class, 'saveSettings'])->name('saveSettings');
    Route::get('/make-consignments', [ConsignmentController::class, 'make_consignments'])->name('make_consignments');
    Route::get('/home', [ConsignmentController::class, 'noSession'])->name('noSession');
    Route::get('/all-consignments', [ConsignmentController::class, 'index'])->name('all-consignments');
});

Route::get('/test', [TestController::class, 'index'])->name('test');