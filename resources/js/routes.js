
import Vue from 'vue';
import VueRouter from 'vue-router';

import Consignments from '../js/components/pages/consignments/index.vue';
import Settings from '../js/components/pages/settings/Index.vue';
import Welcome from '../js/components/pages/welcome/index.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'consignments',
            title: 'Consignments',
            component: Consignments,
            meta : {
                ignoreInMenu : 0
            }
        },
        {
            path: '/settings',
            name: 'settings',
            title: 'Settings',
            component: Settings,
            meta : {
                ignoreInMenu : 0
            }
        }
    ]
});

export default router;
