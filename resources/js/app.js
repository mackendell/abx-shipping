/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import router from './routes';
import App from './components/layouts/index.vue'
import Vue from 'vue'
import VueRouter from 'vue-router'
import PolarisVue from '@hulkapps/polaris-vue';
import '@hulkapps/polaris-vue/dist/polaris-vue.min.css';
Vue.use(PolarisVue);

Vue.use(VueRouter)

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
